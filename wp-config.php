<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_bogazici');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8dF_ahd;ayTWobrNQq9];@6Ux7wu~j|-8z>[T-vSFZlB#Z``#L`cg-L|tEb9Lt l');
define('SECURE_AUTH_KEY',  '~5~oeR*`Z+V`k0a?-!j^BV8wO@_GVKart<%?QMzA7k|-yMO+,S^/i=!5+rpvH.y}');
define('LOGGED_IN_KEY',    'NDkc_-&)Fe7S9v.4,d+=`xf4|>+)4sTRlcS>MJnK;!zGRMZDZm04Ub]S.q_ZYf: ');
define('NONCE_KEY',        'Ml*I!S;|Z3Q.8A2kc2+9v0Zf;~~Y74XMO`_t1}Gl^bp%dN<@4|yh9CEc@0oIx[vp');
define('AUTH_SALT',        '%`,*?Xjk5!e8Z|rNZ>h3R;CK!Z[A-1o*T|&}-A*4_G~t:(ODCVq>vF?%T(@l#c+T');
define('SECURE_AUTH_SALT', 'QPtn;FD1;r|JAHT)oPx?5#e^0j-HN 2|k-V8a>-_OH7QewAzKxWerWC-`|Zx#ee|');
define('LOGGED_IN_SALT',   'jVVUs!)v5Am-OXeqE g6,H2gKD.J?1;[Ig,+F|(g+wSB9o;ExTl/V`i(s;|>oW H');
define('NONCE_SALT',       '`BPg|T:dB435`&R|4I-f/{n G-`9/z_kF,#)JXVl*QRjh3BoknY;F,G8*W(-PJ0k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
